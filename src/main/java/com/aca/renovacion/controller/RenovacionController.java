package com.aca.renovacion.controller;

import java.util.logging.Logger;
import java.util.logging.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aca.renovacion.service.RenovacionServiceImpl;

@RestController
@RequestMapping("/servicios/renovacion")
public class RenovacionController {
	
	 private final static Logger LOGGER = Logger.getLogger(RenovacionController.class.getName());

	@Autowired
	RenovacionServiceImpl renovacionService;
	
	@RequestMapping(method = RequestMethod.GET, value= "/consultarlote/{id_lote}")
	public ResponseEntity<?> consultar_lote(@PathVariable("id_lote")int in) throws Exception{
		LOGGER.log(Level.INFO, "id_lote: "+in);
		try {
			String result = renovacionService.consultar_lote(in);
			
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
		
		
		
	}
	

}
