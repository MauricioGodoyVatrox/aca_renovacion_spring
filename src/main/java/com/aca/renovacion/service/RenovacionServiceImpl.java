package com.aca.renovacion.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class RenovacionServiceImpl implements RenovacionService{
	
	@PersistenceContext
	private EntityManager entityManager;
	
//	EntityManagerFactory emf = Persistence.createEntityManagerFactory("CONSULTAR_LOTE");
//	EntityManager entityManager = emf.createEntityManager();
	
	private final static Logger LOGGER = Logger.getLogger(RenovacionServiceImpl.class.getName());

	@Override
	@Transactional
	public String consultar_lote(int in) throws Exception {
		
		LOGGER.log(Level.INFO, "Request"+in);
		
		StoredProcedureQuery proc  = entityManager.createStoredProcedureQuery("CONSULTAR_LOTE");
		
		proc.registerStoredProcedureParameter("P_ID_LOTE", int.class, ParameterMode.IN);
		proc.registerStoredProcedureParameter("P_RESULTADO", String.class, ParameterMode.OUT);
		proc.setParameter("P_ID_LOTE", in);
		proc.execute();
		String consultarLoteRespose =(String) proc.getOutputParameterValue("P_RESULTADO");
		
		return consultarLoteRespose;
	}

	
}
